package me.hibo98.crypt;

import java.io.UnsupportedEncodingException;

public class Crypt {

    private static String data = "";
    private static int outputlength = 0;

    public static void main(String[] args) throws UnsupportedEncodingException {
        outputlength = Integer.parseInt(args[0]);
        if (outputlength < 128) {
            System.err.println("Ausgabelänge zu kurz (min. 128 Zeichen)");
            System.exit(1);
        }
        for (int i = 1; i < args.length; i++) {
            data += args[i];
        }
        byte[] bytedata = data.getBytes("UTF-8");
        int bytelength = bytedata.length;
        String ende = "";
        int gerade = 2;
        int ungerade = 1;
        int five = 5;
        int three = 3;
        String all = "";
        for (int i = 0; i < bytelength; i++) {
            if (bytedata[i] % 2 == 0) {
                gerade += bytedata[i];
            } else if (bytedata[i] % 5 == 0) {
                five += bytedata[i];
            } else if (bytedata[i] % 3 == 0) {
                three += bytedata[i];
            } else {
                ungerade *= bytedata[i];
            }
            all += Integer.toHexString((int) bytedata[i] * bytelength * bytedata[bytelength - 1] - i);
        }
        ende += Integer.toHexString((int) Math.abs(gerade) * bytelength * (outputlength / 2));
        ende += Integer.toHexString((int) Math.abs(ungerade) * 3);
        ende += Integer.toHexString((int) Math.abs(five) * outputlength * bytelength * 5);
        ende += Integer.toHexString((int) Math.abs(three) * outputlength);
        String gesamt = String.valueOf(all).substring(0, all.length() - bytelength) + ende;
        gesamt = gesamt.replace("-", Integer.toHexString((int) "-".getBytes("UTF-8")[0]));
        while (gesamt.length() < outputlength) {
            gesamt += gesamt;
        }
        System.out.println(gesamt.substring(5, outputlength + 5));
    }
}
